#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# PACE-5000 startup cmd
# -----------------------------------------------------------------------------
require(pace5000)

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
epicsEnvSet("IPADDR",   "192.168.1.33")
epicsEnvSet("IPPORT",   "5025")
epicsEnvSet("SYS",      "SES-DAC-01:")
epicsEnvSet("LOCATION", "$(SYS) $(IPADDR)")
epicsEnvSet("DEV",      "PACE5000-001")
epicsEnvSet("PREFIX",   "$(SYS)$(DEV)")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# E3 Common databases
iocshLoad("$(E3_COMMON_DIR)/e3-common.iocsh")

# PACE 5000
iocshLoad("$(pace5000_DIR)pace5000.iocsh", "PREFIX=$(PREFIX), P=$(SYS), R=$(DEV):, IPADDR=$(IPADDR), IPPORT=$(IPPORT)")
